function getFactorial(num) {
    if (num === 0 || num === 1) {
        return 1;
    } else {
        return num * getFactorial(num - 1);
    }
}

const inputNumber = +prompt('Введіть число');
const result = getFactorial(inputNumber);
console.log(result);